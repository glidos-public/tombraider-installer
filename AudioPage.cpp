// AudioPage.cpp : implementation file
//

#include "stdafx.h"
#include "TRinst.h"
#include "AudioPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAudioPage property page

IMPLEMENT_DYNCREATE(CAudioPage, CPropertyPage)

CAudioPage::CAudioPage() : CPropertyPage(CAudioPage::IDD)
{
	//{{AFX_DATA_INIT(CAudioPage)
	m_selection = 0;
	//}}AFX_DATA_INIT
}

CAudioPage::~CAudioPage()
{
}

void CAudioPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAudioPage)
	DDX_Radio(pDX, IDC_RADIO1, m_selection);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAudioPage, CPropertyPage)
	//{{AFX_MSG_MAP(CAudioPage)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAudioPage message handlers

BOOL CAudioPage::OnSetActive() 
{
    UpdateData(FALSE);

    ((CPropertySheet *)GetParent())->SetWizardButtons(PSWIZB_NEXT);
	
	return CPropertyPage::OnSetActive();
}

BOOL CAudioPage::OnKillActive() 
{
    LPCSTR fmt =
        "\r\n"
        "[DIGITAL]\r\n"
        "DeviceName  = %s\r\n"
        "DeviceIRQ   = %d\r\n"
        "DeviceDMA   = %d\r\n"
        "DevicePort  = 0x%x\r\n"
        "DeviceID    = 0x%x\r\n"
        "\r\n"
        "[MIDI]\r\n"
        "DeviceName  = No MIDI Device            \r\n"
        "DevicePort  = 0xffffffff\r\n"
        "DeviceID    = 0xffffffff\r\n"
        "\r\n";
    LPCSTR name[3] =
    {
        "No Digital Device         ",
        "Sound Blaster 16/AWE32",
        "Sound Blaster Pro"
    };
    int irq[3]  = {-1, 7, 5};
    int dma[3]  = {-1, 1, 1};
    int port[3] = {0xffffffff, 0x220, 0x220};
    int id[3]   = {0xffffffff, 0xe016, 0xe001};

    UpdateData();

    CString contents;
    contents.Format(fmt, name[m_selection],
                         irq[m_selection],
                         dma[m_selection],
                         port[m_selection],
                         id[m_selection]);

    CFile file;

    if(!file.Open("C:\\TOMBRAID\\HMISET.CFG", CFile::modeCreate|CFile::modeWrite))
    {
        AfxMessageBox("Couldn't open file");
        goto failure;
    }

    file.Write((LPCSTR)contents, contents.GetLength());

    file.Close();

    if(m_selection == 1)
        AfxMessageBox("Remember to download and install VDMSound");

failure:
	return CPropertyPage::OnKillActive();
}
