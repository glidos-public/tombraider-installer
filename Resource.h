//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by TRinst.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TRINST_DIALOG               102
#define IDD_WELCOME_PAGE                103
#define IDD_BACKUP_PAGE                 104
#define IDD_COPY_PAGE                   105
#define IDD_VOODOO                      106
#define IDR_MAINFRAME                   128
#define IDD_AUDIO_PAGE                  130
#define IDD_FINISH_PAGE                 131
#define IDC_BACKUP                      1001
#define IDC_INSTALL                     1002
#define IDC_EDIT1                       1003
#define IDC_PROGRESS1                   1004
#define IDC_DOWNLOAD                    1005
#define IDC_RADIO1                      1006
#define IDC_RADIO2                      1007
#define IDC_CHECK1                      1007
#define IDC_RADIO3                      1008

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
