// ReportingSession.h: interface for the CReportingSession class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_REPORTINGSESSION_H__722E2D48_2FF4_4AE7_8D4B_152CE0033822__INCLUDED_)
#define AFX_REPORTINGSESSION_H__722E2D48_2FF4_4AE7_8D4B_152CE0033822__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DownloadThread.h"

class CReportingSession : public CInternetSession  
{
private:
    CDownloadThread *m_dthread;
public:
	CReportingSession(CDownloadThread *dthread);
	virtual ~CReportingSession();
    virtual void OnStatusCallback(DWORD  dwContext,
                                  DWORD  dwInternetStatus,
                                  LPVOID lpvStatusInformation,
                                  DWORD  dwStatusInformationLength);
};

#endif // !defined(AFX_REPORTINGSESSION_H__722E2D48_2FF4_4AE7_8D4B_152CE0033822__INCLUDED_)
