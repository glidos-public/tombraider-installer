// InstallThread.cpp: implementation of the CInstallThread class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TRinst.h"
#include "InstallThread.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CInstallThread::CInstallThread()
{
    m_done      = false;
    m_steps     = 0;
}

CInstallThread::~CInstallThread()
{

}

void CInstallThread::Execute()
{
    const char *files[] =
    {
        "DOS4GW.EXE",
        "HMIDET.386",
        "HMIDRV.386",
        "HMISET.CFG",
        "TOMB.EXE",
        "TOMB.SP",
        NULL
    };
    const char  *inst;
    const char  *test_file = "data\\level10c.phd";
    CFileFind finder;
    char         drive;
    CString      cd;
    CString      folder = "C:\\TOMBRAID";
    bool         found = false;
    int          i;
    CString      report;

    SendReport("Creating folder");
    Sleep(100);

    if(!::CreateDirectory((LPCSTR)folder, NULL))
    {
        AfxMessageBox("Failer to create installation folder");
        return;
    }

    folder += '\\';

    /* Insert sleep after report just to slow things down
     * a bit and make the user feedback readable. */
    SendReport("Finding Tomb Raider CD");
    Sleep(100);

    for(drive = 'D'; drive <= 'Z' && !found; drive += 1)
    {
        cd.Format("%c:\\", drive);
        
        if(finder.FindFile((LPCSTR)(cd+test_file)))
            found = true;
    }

    if(!found)
    {
        AfxMessageBox("Tomb Raider CD not found");
        return;
    }

    for(i = 0; files[i] != NULL; i++)
    {
        report.Format("Copying: %s", files[i]);
        SendReport((LPCSTR)report);
        Sleep(100);

        if(!CopyFile((LPCSTR)(cd+files[i]), (LPCSTR)(folder+files[i])))
        {
            AfxMessageBox("Failed to copy file");
            return;
        }
    }

    if(finder.FindFile((LPCSTR)(cd+"INSTALL.EXE")))
    {
        inst = "INSTALL.EXE";
    }
    else if(finder.FindFile((LPCSTR)(cd+"INSTALLT.EXE")))
    {
        inst = "INSTALLT.EXE";
    }
    else
    {
        AfxMessageBox("Can't find INSTALL.EXE or INSTALLT.EXE");
        return;
    }

    report.Format("Copying %s to SETUP.EXE", (LPCSTR)inst);
    SendReport((LPCSTR)report);
    Sleep(100);

    if(!CopyFile((LPCSTR)(cd+inst), (LPCSTR)(folder+"SETUP.EXE")))
    {
        AfxMessageBox("Failed to copy file");
        return;
    }

    SendReport("Done");
    Sleep(100);

    m_done = true;
}

void CInstallThread::SendReport(LPCSTR report)
{
    CSingleLock lock(&m_data_lock, true);

    m_report = report;

    m_steps += 1;

    Report();
}

CString CInstallThread::GetReport()
{
    CSingleLock lock(&m_data_lock, true);

    return m_report;
}

int CInstallThread::GetProgress()
{
    CSingleLock lock(&m_data_lock, true);

    return (int)(m_steps * 100.0 / 11);
}

BOOL CInstallThread::CopyFile(LPCSTR from, LPCSTR to)
{
    CFile fromfile, tofile;
    BYTE  buf[1024];
    UINT  n;

    if(!fromfile.Open(from, CFile::modeRead))
        return FALSE;

    if(!tofile.Open(to, CFile::modeCreate | CFile::modeWrite))
        return FALSE;

    n = 1024;
    while(n >= 1024)
    {
        n = fromfile.Read(buf, 1024);

        if(n > 0)
            tofile.Write(buf, n);
    }

    return TRUE;
}
