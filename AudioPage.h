#if !defined(AFX_AUDIOPAGE_H__C7AFF49B_949A_45B8_BCE2_96A6D4F56A9F__INCLUDED_)
#define AFX_AUDIOPAGE_H__C7AFF49B_949A_45B8_BCE2_96A6D4F56A9F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AudioPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAudioPage dialog

class CAudioPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CAudioPage)

// Construction
public:
	CAudioPage();
	~CAudioPage();

// Dialog Data
	//{{AFX_DATA(CAudioPage)
	enum { IDD = IDD_AUDIO_PAGE };
	int		m_selection;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CAudioPage)
	public:
	virtual BOOL OnSetActive();
	virtual BOOL OnKillActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CAudioPage)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AUDIOPAGE_H__C7AFF49B_949A_45B8_BCE2_96A6D4F56A9F__INCLUDED_)
