// BackupPage.cpp : implementation file
//

#include "stdafx.h"
#include "TRinst.h"
#include "BackupPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBackupPage property page

IMPLEMENT_DYNCREATE(CBackupPage, CPropertyPage)

CBackupPage::CBackupPage() : CPropertyPage(CBackupPage::IDD)
{
	//{{AFX_DATA_INIT(CBackupPage)
	m_edit = _T("");
	//}}AFX_DATA_INIT
}

CBackupPage::~CBackupPage()
{
}

void CBackupPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBackupPage)
	DDX_Control(pDX, IDC_BACKUP, m_button);
	DDX_Text(pDX, IDC_EDIT1, m_edit);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBackupPage, CPropertyPage)
	//{{AFX_MSG_MAP(CBackupPage)
	ON_BN_CLICKED(IDC_BACKUP, OnBackup)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBackupPage message handlers

BOOL CBackupPage::OnSetActive() 
{
	CFileFind finder;

	if(!finder.FindFile("C:\\TOMBRAID"))
	    return FALSE;

	((CPropertySheet *)GetParent())->SetWizardButtons(0);

    return CPropertyPage::OnSetActive();
}

void CBackupPage::OnBackup() 
{
	int i = 0;
	CFileFind finder;
	CString new_folder;

	new_folder.Format("C:\\TOMBRAID%d", i);
	while(finder.FindFile((LPCSTR)new_folder))
	{
		i += 1;
		new_folder.Format("C:\\TOMBRAID%d", i);
	}

	if(::MoveFile("C:\\TOMBRAID", (LPCSTR)new_folder))
	{
        m_edit = "Done";
        UpdateData(FALSE);
		m_button.EnableWindow(FALSE);
        ((CPropertySheet *)GetParent())->SetWizardButtons(PSWIZB_NEXT);
	}
	else
	{
        AfxMessageBox("Move operation failed");
	}
}
