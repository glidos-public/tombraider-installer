/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * Glide server
 * 
 * $Id$
 * $Date$
 *
 * $Log$
 * Revision 1.1  2003/03/04 00:42:13  paul
 * First version of Tomb Raider installer
 *
 * Revision 1.2  2001/06/20 16:17:01  paul
 * Send "abort" command to VXD on shutdown to stop the server thread
 * hanging in a get call.
 *
 * Add buttons for shutdown, and saving the edit box.
 *
 * Us commands in protocol, only GrGlideInit at this stage.
 *
 ************************************************************/

// Thread.h: interface for the CThread class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_THREAD_H__FC98E020_BB88_11D3_ABD0_00C0DFF0F563__INCLUDED_)
#define AFX_THREAD_H__FC98E020_BB88_11D3_ABD0_00C0DFF0F563__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#define WM_USER_KILL_THREAD (WM_USER + 0x1001)

class CThread  
{
private:
	bool m_dying;
	CWinThread *m_thread;
    HANDLE m_report_semaphore;
public:
	bool Dying();
	void Wait();
    bool Monitor();
	void Kill();
	void Start();
	CThread();
	virtual ~CThread();

private:
	static UINT Hook(LPVOID arg);
	virtual void Execute()=0;

protected:
    void Report();
};

#endif // !defined(AFX_THREAD_H__FC98E020_BB88_11D3_ABD0_00C0DFF0F563__INCLUDED_)
