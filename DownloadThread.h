// DownloadThread.h: interface for the CDownloadThread class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DOWNLOADTHREAD_H__70BDC52C_2118_416A_A2E8_E3EE5CDCF4A8__INCLUDED_)
#define AFX_DOWNLOADTHREAD_H__70BDC52C_2118_416A_A2E8_E3EE5CDCF4A8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Thread.h"

class CDownloadThread : public CThread  
{
private:
    CCriticalSection   m_data_lock;
    CHttpFile         *m_file;
    CString            m_report;
    int                m_steps;
    int                m_progress;
public:
	void Kill();
	void SendInetReport(DWORD dwInternetStatus);
	bool m_done;
	CDownloadThread();
	virtual ~CDownloadThread();
    CString GetReport();
    int     GetProgress();

private:
	void Execute();
    void SendReport(LPCSTR report);
};

#endif // !defined(AFX_DOWNLOADTHREAD_H__70BDC52C_2118_416A_A2E8_E3EE5CDCF4A8__INCLUDED_)
