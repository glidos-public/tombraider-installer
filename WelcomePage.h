#if !defined(AFX_WELCOMEPAGE_H__AFF7D9DD_BA23_4F49_859E_96E36D0984CF__INCLUDED_)
#define AFX_WELCOMEPAGE_H__AFF7D9DD_BA23_4F49_859E_96E36D0984CF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WelcomePage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CWelcomePage dialog

class CWelcomePage : public CPropertyPage
{
	DECLARE_DYNCREATE(CWelcomePage)

// Construction
public:
	CWelcomePage();
	~CWelcomePage();

// Dialog Data
	//{{AFX_DATA(CWelcomePage)
	enum { IDD = IDD_WELCOME_PAGE };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CWelcomePage)
	public:
	virtual BOOL OnSetActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CWelcomePage)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WELCOMEPAGE_H__AFF7D9DD_BA23_4F49_859E_96E36D0984CF__INCLUDED_)
