// TRinst.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TRinst.h"
#include "WelcomePage.h"
#include "BackupPage.h"
#include "CopyPage.h"
#include "AudioPage.h"
#include "VoodooDownload.h"
#include "FinishPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTRinstApp

BEGIN_MESSAGE_MAP(CTRinstApp, CWinApp)
	//{{AFX_MSG_MAP(CTRinstApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTRinstApp construction

CTRinstApp::CTRinstApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CTRinstApp object

CTRinstApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CTRinstApp initialization

BOOL CTRinstApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	CPropertySheet  sheet;

	CWelcomePage    welcome;
	CBackupPage     backup;
	CCopyPage       copy;
    CAudioPage      audio;
    CVoodooDownload download;
    CFinishPage     finish;

	sheet.AddPage(&welcome);
	sheet.AddPage(&backup);
	sheet.AddPage(&copy);
	sheet.AddPage(&audio);
	sheet.AddPage(&download);
	sheet.AddPage(&finish);

	sheet.SetWizardMode();

	m_pMainWnd = &sheet;
	int nResponse = sheet.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
