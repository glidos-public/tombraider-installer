// InstallThread.h: interface for the CInstallThread class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INSTALLTHREAD_H__A149330D_503A_44A9_901D_14D32B7DA5F7__INCLUDED_)
#define AFX_INSTALLTHREAD_H__A149330D_503A_44A9_901D_14D32B7DA5F7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Thread.h"

class CInstallThread : public CThread  
{
private:
    CCriticalSection m_data_lock;
    CString          m_report;
    int              m_steps;
    int              m_progress;
public:
	bool m_done;
	CInstallThread();
	virtual ~CInstallThread();
    CString GetReport();
    int     GetProgress();

private:
	BOOL CopyFile(LPCSTR from, LPCSTR to);
	void Execute();
    void SendReport(LPCSTR report);
};

#endif // !defined(AFX_INSTALLTHREAD_H__A149330D_503A_44A9_901D_14D32B7DA5F7__INCLUDED_)
