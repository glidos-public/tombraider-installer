// VoodooDownload.cpp : implementation file
//

#include "stdafx.h"
#include "TRinst.h"
#include "DownloadThread.h"
#include "VoodooDownload.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CVoodooDownload property page

IMPLEMENT_DYNCREATE(CVoodooDownload, CPropertyPage)

CVoodooDownload::CVoodooDownload() : CPropertyPage(CVoodooDownload::IDD)
{
	//{{AFX_DATA_INIT(CVoodooDownload)
	m_edit = _T("");
	//}}AFX_DATA_INIT
}

CVoodooDownload::~CVoodooDownload()
{
}

void CVoodooDownload::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CVoodooDownload)
	DDX_Control(pDX, IDC_PROGRESS1, m_progress);
	DDX_Control(pDX, IDC_DOWNLOAD, m_button);
	DDX_Text(pDX, IDC_EDIT1, m_edit);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CVoodooDownload, CPropertyPage)
	//{{AFX_MSG_MAP(CVoodooDownload)
	ON_BN_CLICKED(IDC_DOWNLOAD, OnDownload)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVoodooDownload message handlers

void CVoodooDownload::OnDownload() 
{
    ((CPropertySheet *)GetParent())->SetWizardButtons(0);
    m_button.EnableWindow(FALSE);

    m_thread.Start();

    while(m_thread.Monitor())
    {
        m_progress.SetPos(m_thread.GetProgress());
        m_edit = m_thread.GetReport();
        UpdateData(FALSE);
    }

    if(m_thread.m_done)
    {
        ((CPropertySheet *)GetParent())->SetWizardButtons(PSWIZB_NEXT);
        m_progress.SetPos(0);
		m_button.EnableWindow(FALSE);
    }
    else
    {
        m_button.EnableWindow(TRUE);
    }
}

BOOL CVoodooDownload::OnSetActive() 
{
    m_progress.SetRange(0, 100);

    ((CPropertySheet *)GetParent())->SetWizardButtons(PSWIZB_NEXT);
	
	return CPropertyPage::OnSetActive();
}

void CVoodooDownload::OnCancel() 
{
	// TODO: Add your specialized code here and/or call the base class
	m_thread.Kill();

	CPropertyPage::OnCancel();
}
