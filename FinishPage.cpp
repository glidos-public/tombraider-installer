// FinishPage.cpp : implementation file
//

#include "stdafx.h"
#include "TRinst.h"
#include "FinishPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFinishPage property page

IMPLEMENT_DYNCREATE(CFinishPage, CPropertyPage)

CFinishPage::CFinishPage() : CPropertyPage(CFinishPage::IDD)
{
	//{{AFX_DATA_INIT(CFinishPage)
	m_checked = TRUE;
	//}}AFX_DATA_INIT
}

CFinishPage::~CFinishPage()
{
}

void CFinishPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFinishPage)
	DDX_Check(pDX, IDC_CHECK1, m_checked);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFinishPage, CPropertyPage)
	//{{AFX_MSG_MAP(CFinishPage)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFinishPage message handlers

BOOL CFinishPage::OnSetActive() 
{
    ((CPropertySheet *)GetParent())->SetWizardButtons(PSWIZB_FINISH);
	
	return CPropertyPage::OnSetActive();
}


BOOL CFinishPage::OnWizardFinish() 
{
    UpdateData();

    if(m_checked)
        ::ShellExecute(m_hWnd, "open", "http://www.glidos.net", NULL, NULL, SW_SHOWNORMAL);
		
	return CPropertyPage::OnWizardFinish();
}
