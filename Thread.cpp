/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * Glide server
 * 
 * $Id$
 * $Date$
 *
 * $Log$
 * Revision 1.1  2003/03/04 00:42:13  paul
 * First version of Tomb Raider installer
 *
 * Revision 1.2  2001/06/20 16:17:01  paul
 * Send "abort" command to VXD on shutdown to stop the server thread
 * hanging in a get call.
 *
 * Add buttons for shutdown, and saving the edit box.
 *
 * Us commands in protocol, only GrGlideInit at this stage.
 *
 ************************************************************/

// Thread.cpp: implementation of the CThread class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Thread.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CThread::CThread()
{
	m_thread = NULL;
	m_dying = false;
    m_report_semaphore = ::CreateSemaphore(NULL, 0, 1, NULL);
}

CThread::~CThread()
{
	if(m_thread)
	{
		Kill();
		Wait();
		delete m_thread;
	}

    ::CloseHandle(m_report_semaphore);
}

void CThread::Start()
{
    if(m_thread)
    {
        delete m_thread;
        m_thread = NULL;
    }
	m_thread = AfxBeginThread(Hook, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	m_thread->m_bAutoDelete = false;
	m_thread->ResumeThread();
}

void CThread::Kill()
{
	m_dying = true;
	if(m_thread) ::PostThreadMessage(m_thread->m_nThreadID, WM_USER_KILL_THREAD, 0, 0);
}

bool CThread::Monitor()
{
	MSG msg;
    bool going;
    bool report;
    HANDLE handles[2] =
    {
        m_thread->m_hThread,
        m_report_semaphore
    };

	going = (m_thread != NULL);

	while(going)
	{
		while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			DispatchMessage(&msg);

        DWORD val = MsgWaitForMultipleObjects(2, handles, false, INFINITE, QS_ALLINPUT);
        
        report = (val == WAIT_OBJECT_0 + 1);
        going  = !(report || val == WAIT_OBJECT_0);
	}

    return report;
}

void CThread::Wait()
{
    while(Monitor())
        ;
}

UINT CThread::Hook(void *arg)
{
	/* AfxSocketInit(); */
	((CThread *) arg)->Execute();
	return 0;
}

bool CThread::Dying()
{
	return m_dying;
}

void CThread::Report()
{
    ::ReleaseSemaphore(m_report_semaphore, 1, NULL);
}
