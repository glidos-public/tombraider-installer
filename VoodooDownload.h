#if !defined(AFX_VOODOODOWNLOAD_H__0323C775_F508_41E1_965B_9E15C9B83281__INCLUDED_)
#define AFX_VOODOODOWNLOAD_H__0323C775_F508_41E1_965B_9E15C9B83281__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// VoodooDownload.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CVoodooDownload dialog

#include "DownloadThread.h"

class CVoodooDownload : public CPropertyPage
{
	DECLARE_DYNCREATE(CVoodooDownload)

// Construction
public:
	CVoodooDownload();
	~CVoodooDownload();

// Dialog Data
	//{{AFX_DATA(CVoodooDownload)
	enum { IDD = IDD_VOODOO };
	CProgressCtrl	m_progress;
	CButton	m_button;
	CString	m_edit;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CVoodooDownload)
	public:
	virtual BOOL OnSetActive();
	virtual void OnCancel();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CVoodooDownload)
	afx_msg void OnDownload();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
    CDownloadThread m_thread;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VOODOODOWNLOAD_H__0323C775_F508_41E1_965B_9E15C9B83281__INCLUDED_)
