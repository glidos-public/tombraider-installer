#if !defined(AFX_BACKUPPAGE_H__EDE548D7_0D45_4DDB_8C49_E45CA46690D7__INCLUDED_)
#define AFX_BACKUPPAGE_H__EDE548D7_0D45_4DDB_8C49_E45CA46690D7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BackupPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBackupPage dialog

class CBackupPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CBackupPage)

// Construction
public:
	CBackupPage();
	~CBackupPage();

// Dialog Data
	//{{AFX_DATA(CBackupPage)
	enum { IDD = IDD_BACKUP_PAGE };
	CButton	m_button;
	CString	m_edit;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CBackupPage)
	public:
	virtual BOOL OnSetActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CBackupPage)
	afx_msg void OnBackup();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BACKUPPAGE_H__EDE548D7_0D45_4DDB_8C49_E45CA46690D7__INCLUDED_)
