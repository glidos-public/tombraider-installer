// DownloadThread.cpp: implementation of the CDownloadThread class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TRinst.h"
#include "DownloadThread.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include "ReportingSession.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDownloadThread::CDownloadThread()
{
    m_done      = false;
    m_progress  = 0;
    m_file      = NULL;
}

CDownloadThread::~CDownloadThread()
{

}

void CDownloadThread::Execute()
{
    try
    {
        CReportingSession  sess(this);
        CHttpConnection   *http;
        CFile              ofile;
        BYTE               buf[1024];
        DWORD              n, len, rcvd;

        SendReport("Starting download");
        http = sess.GetHttpConnection("www.laserpoint.freeserve.co.uk");

        {
            CSingleLock lock(&m_data_lock, true);

            m_file = http->OpenRequest("GET", "Tomb.exe", NULL, 1, NULL, NULL, INTERNET_FLAG_RELOAD);
        }

        m_file->SendRequest();

        CString slen;
        len = 1024;
        if(m_file->QueryInfo(HTTP_QUERY_CONTENT_LENGTH, slen))
            len = atoi((LPCSTR)slen);

        if(!ofile.Open("C:\\TOMBRAID\\TOMB.EXE", CFile::modeCreate | CFile::modeWrite))
        {
            AfxMessageBox("Cannot open disc file for writing");
            return;
        }

        SendReport("Receiving file");

        n    = 1024;
        rcvd = 0;
        while(n >= 1024)
        {
            n = m_file->Read(buf, 1024);

            if(n > 0)
                ofile.Write(buf, n);

            rcvd += n;
            m_progress = (int)(rcvd * 100.0 / len);
            SendReport("Receiving file");
        }

        {
            CSingleLock lock(&m_data_lock, true);

            if(m_file != NULL)
            {
                m_file->Close();

                m_file = NULL;
            }
        }

        SendReport("Done");

        m_done = true;
    }
    catch(CInternetException *e)
    {
        char buf[256];
        e->GetErrorMessage(buf, 256);
        e->Delete();
        CString s;
        s.Format("Error: %s", buf);
        SendReport((LPCSTR)s);
    }
}

void CDownloadThread::SendReport(LPCSTR report)
{
    CSingleLock lock(&m_data_lock, true);

    m_report = report;

    Report();
}

CString CDownloadThread::GetReport()
{
    CSingleLock lock(&m_data_lock, true);

    return m_report;
}

int CDownloadThread::GetProgress()
{
    CSingleLock lock(&m_data_lock, true);

    return m_progress;
}

void CDownloadThread::SendInetReport(DWORD dwInternetStatus)
{
    LPCSTR msg;
    bool   send = true;

    switch(dwInternetStatus)
    {
    case INTERNET_STATUS_RESOLVING_NAME:
        msg = "Looking up the IP address";
        break;
    case INTERNET_STATUS_NAME_RESOLVED:
        msg = "Found the IP address";
        break;
        
    case INTERNET_STATUS_CONNECTING_TO_SERVER:
        msg = "Connecting to the server";
        break;
        
    case INTERNET_STATUS_CONNECTED_TO_SERVER:
        msg = "Connected to the server";
        break;
        
    case INTERNET_STATUS_SENDING_REQUEST:
        msg = "Sending the request to the server";
        break;
        
    case INTERNET_STATUS_REQUEST_SENT:
        msg = "Request sent";
        break;
        
    case INTERNET_STATUS_RECEIVING_RESPONSE:
        msg = "Receiving response";
        send = false;
        break;
        
    case INTERNET_STATUS_RESPONSE_RECEIVED:
        msg = "Response received";
        send = false;
        break;
        
    case INTERNET_STATUS_CLOSING_CONNECTION:
        msg = "Closing the connection";
        break;
        
    case INTERNET_STATUS_CONNECTION_CLOSED:
        msg = "Connection closed";
        break;
        
    case INTERNET_STATUS_HANDLE_CREATED:
        msg = "Handle created";
        send = false;
        break;
        
    case INTERNET_STATUS_HANDLE_CLOSING:
        msg = "Handle closed";
        send = false;
        break;
        
    default:
        msg = "Unknown internet message";
        break;
    }

    if(send)
        SendReport(msg);
}

void CDownloadThread::Kill()
{
    CSingleLock lock(&m_data_lock, true);

    if(m_file != NULL)
        ::InternetCloseHandle((HINTERNET)(*m_file));
}
