// ReportingSession.cpp: implementation of the CReportingSession class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TRinst.h"
#include "ReportingSession.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CReportingSession::CReportingSession(CDownloadThread *dthread)
{
    m_dthread = dthread;

    EnableStatusCallback(TRUE);
}

CReportingSession::~CReportingSession()
{
    EnableStatusCallback(FALSE);

    Close();
}

void CReportingSession::OnStatusCallback(DWORD dwContext,
                                         DWORD dwInternetStatus,
                                         LPVOID lpvStatusInformation,
                                         DWORD dwStatusInformationLength)
{
    AFX_MANAGE_STATE(AfxGetAppModuleState());

    m_dthread->SendInetReport(dwInternetStatus);
}
