// CopyPage.cpp : implementation file
//

#include "stdafx.h"
#include "TRinst.h"
#include "InstallThread.h"
#include "CopyPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCopyPage property page

IMPLEMENT_DYNCREATE(CCopyPage, CPropertyPage)

CCopyPage::CCopyPage() : CPropertyPage(CCopyPage::IDD)
{
	//{{AFX_DATA_INIT(CCopyPage)
	m_edit = _T("");
	//}}AFX_DATA_INIT
}

CCopyPage::~CCopyPage()
{
}

void CCopyPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCopyPage)
	DDX_Control(pDX, IDC_INSTALL, m_button);
	DDX_Control(pDX, IDC_PROGRESS1, m_progress);
	DDX_Text(pDX, IDC_EDIT1, m_edit);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCopyPage, CPropertyPage)
	//{{AFX_MSG_MAP(CCopyPage)
	ON_BN_CLICKED(IDC_INSTALL, OnInstall)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCopyPage message handlers

void CCopyPage::OnInstall() 
{
    CInstallThread thread;
    
    m_button.EnableWindow(FALSE);

    thread.Start();

    while(thread.Monitor())
    {
        m_progress.SetPos(thread.GetProgress());
        m_edit = thread.GetReport();
        UpdateData(FALSE);
    }

    if(thread.m_done)
    {
        m_progress.SetPos(0);
        ((CPropertySheet *)GetParent())->SetWizardButtons(PSWIZB_NEXT);
    }
    else
    {
		m_button.EnableWindow(TRUE);
    }
}

BOOL CCopyPage::OnSetActive() 
{
    m_progress.SetRange(0, 100);

    ((CPropertySheet *)GetParent())->SetWizardButtons(0);
	
	return CPropertyPage::OnSetActive();
}

