#if !defined(AFX_COPYPAGE_H__90C1336B_B1AB_4DBC_98A8_64A30F8648EA__INCLUDED_)
#define AFX_COPYPAGE_H__90C1336B_B1AB_4DBC_98A8_64A30F8648EA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CopyPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCopyPage dialog

class CCopyPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CCopyPage)

// Construction
public:
	CCopyPage();
	~CCopyPage();

// Dialog Data
	//{{AFX_DATA(CCopyPage)
	enum { IDD = IDD_COPY_PAGE };
	CButton	m_button;
	CProgressCtrl	m_progress;
	CString	m_edit;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CCopyPage)
	public:
	virtual BOOL OnSetActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CCopyPage)
	afx_msg void OnInstall();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COPYPAGE_H__90C1336B_B1AB_4DBC_98A8_64A30F8648EA__INCLUDED_)
